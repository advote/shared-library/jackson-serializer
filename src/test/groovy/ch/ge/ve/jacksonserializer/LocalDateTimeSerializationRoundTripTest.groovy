/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - jackson-serializer                                                                             -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.jacksonserializer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.time.LocalDateTime
import spock.lang.Specification

class LocalDateTimeSerializationRoundTripTest extends Specification {

  private ObjectMapper objectMapper

  def setup() {
    SimpleModule module = new SimpleModule()
    module.addSerializer(LocalDateTime.class, JSDates.SERIALIZER)
    module.addDeserializer(LocalDateTime.class, JSDates.DESERIALIZER)
    objectMapper = new ObjectMapper().registerModule(module)
  }

  def "Should serialize and deserialize date-time values"() {
    given:
    def now = LocalDateTime.now()

    when:
    def serialized = objectMapper.writeValueAsString(now)
    def deserialized = objectMapper.readValue(serialized, LocalDateTime.class)

    then:
    deserialized == now
    serialized.matches("\"JSDate\\[[0-9:\\.\\s]+\\]\"")
  }
}
