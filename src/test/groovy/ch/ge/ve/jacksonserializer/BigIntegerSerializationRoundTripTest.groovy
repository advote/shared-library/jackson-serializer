/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - jackson-serializer                                                                             -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.jacksonserializer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import spock.lang.Specification

class BigIntegerSerializationRoundTripTest extends Specification {

  private ObjectMapper objectMapper

  def setup() {
    SimpleModule module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    objectMapper = new ObjectMapper().registerModule(module)
  }

  def "Should serialize and deserialize BigInteger values"() {
    given:
    def value = BigInteger.valueOf(2L);

    when:
    def serialized = objectMapper.writeValueAsString(value)
    def deserialized = objectMapper.readValue(serialized, BigInteger.class)

    then:
    serialized == '"Ag=="'
    deserialized == value
  }
}
